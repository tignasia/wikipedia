package wikipedia

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD

/**
  * Created by tomasz on 4/9/2017.
  */

case class Event(organizer: String, name: String, budget : Int)

object Main extends App{

  //WikipediaRanking.langs.foreach { lang => println( lang + " : " + WikipediaRanking.occurrencesOfLang(lang,WikipediaRanking.wikiRdd)) }

 // println(WikipediaRanking.rankLangs(WikipediaRanking.langs,WikipediaRanking.wikiRdd))

 //val list = WikipediaRanking.makeIndex(WikipediaRanking.langs,WikipediaRanking.wikiRdd).countByKey().toList
  //list.foreach(println(_))

 //println(WikipediaRanking.rankLangsUsingIndex(WikipediaRanking.makeIndex(WikipediaRanking.langs,WikipediaRanking.wikiRdd)))
 //println(WikipediaRanking.rankLangsReduceByKey(WikipediaRanking.langs,WikipediaRanking.wikiRdd))

   // println(s"Occurences of ${WikipediaRanking.occurrencesOfLang(_,WikipediaRanking.wikiRdd)}")
  //  }

  //val  wikipediaRanking = WikipediaRanking



 val conf: SparkConf = new SparkConf().setAppName("Events").setMaster("local").set("spark.executor.memory","512m");
 val sc: SparkContext = new SparkContext(conf)
 val events = List(Event("My Org","Biba",1000),Event("My Org","Event firmowy",20000),Event("My Org","Spotkanie z kientami",5000),Event("Big Company","Big Event",250000),Event("Big Company","Even Bigger Event",375000))
 val eventsRDD = sc.parallelize(events/*,4*/).map(event => (event.organizer,event.budget)).cache()

 val groupedEventsRDD = eventsRDD.groupByKey()
 println("Group By Key")
 groupedEventsRDD.collect().foreach(println)
 val totalBudgetsRDD = eventsRDD.reduceByKey(_ + _) // group by key + reduce
 println("Reduce By Key (sum)")
 totalBudgetsRDD.collect().foreach(println)

 val duplicatedEventsRDD  = eventsRDD.mapValues(_ * 2).reduceByKey(_ + _)
 println("Reduce By Key (sum duplicated)")
 duplicatedEventsRDD.collect().foreach(println)

 val counts = eventsRDD.countByKey()
 println("Count of events produced by countByKey")
 counts.foreach(println)

// val budgetRDD = sc.parallelize(events/*,4*/).map(event => (event.budget,event))

 // Calculate average budget per event organizer
 val intermediate = eventsRDD.mapValues(b => (b,1)).reduceByKey((val1,val2) => (val1._1 + val2._1, val1._2 + val2._2)) // values becomes pair
 val avgBudgets = intermediate.mapValues{ case (budget : Int,numEvents: Int) => budget / numEvents }
 println("Avg budgets")
 avgBudgets.collect().foreach(println)

 val keysRDD = eventsRDD.keys
 println("Keys")
 keysRDD.collect().foreach(println)





 // Hint: use a combination of `sc.textFile`, `WikipediaData.filePath` and `WikipediaData.parse`
 //val wikiRdd: RDD[WikipediaArticle] = sc.textFile(WikipediaData.filePath,4).map(WikipediaData.parse(_)).cache()

}
